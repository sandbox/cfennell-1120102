Installation
------------

Download module into your modules directory and then visit the modules 
configuration page.  Look for the "MHS" section and enable module.


Configuration
------------

Visit the "manage fields" section of an entity (e.g. a content type) and 
select the "MHS Image Collection Item URL" field.


Usage
------------

Locate an MHS image item and past its url into the MHS Image Collection Item 
URL field. e.g., http://collections.mnhs.org/cms/display.php?irn=10275226 
and then submit your form.